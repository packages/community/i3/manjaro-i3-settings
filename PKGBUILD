# Maintainer: Bernhard Landauer <oberon@manjaro.org>

pkgname=manjaro-i3-settings
pkgbase=manjaro-i3-settings
pkgver=20240224
pkgrel=1
arch=('any')
url="https://gitlab.manjaro.org/profiles-and-settings/desktop-settings"
license=('GPL-3.0-or-later')
pkgdesc='Manjaro Linux i3 settings'
depends=('adapta-maia-theme'
        'conky-i3'
        'dmenu-manjaro'
        'dunst'
        'gsfonts'
        'i3-default-artwork'
        'i3exit'
        'i3-scripts'
        'i3-scrot'
        'i3-gaps'
        'i3-help'
        'i3status'
        'manjaro-base-skel'
        'ttf-terminus-nerd'
        'nitrogen'
        'papirus-maia-icon-theme'
        'picom'
        'urxvt-perls'
        'xcursor-breeze'
        'xorg-fonts-misc'
        'xorg-mkfontscale')
makedepends=('git')
optdepends=('artwork-i3: Manjaro-i3 wallpapers'
            'manjaro-backgrounds: Collection of Manjaro wallpapers')
conflicts=('manjaro-desktop-settings')
provides=('manjaro-desktop-settings')
replaces=('manjaro-i3-settings-dev')
_commit=661a00d2309a13cd38ac1e8fc34459eeaf5a89d6  # branch/master
source=("git+$url.git#commit=${_commit}")
sha256sums=('a424eb644908c27750cc1e2c51c40204204c296f6a053d2646dbd9cde95569cb')

pkgver() {
    date +%Y%m%d
}

package_manjaro-i3-settings() {
    groups=('i3-manjaro')

    cd desktop-settings
    install -d "$pkgdir"/etc
    cp -r community/i3/skel "$pkgdir"/etc
    install -d "$pkgdir"/usr/share/glib-2.0/schemas
    cp community/i3/schemas/* "$pkgdir"/usr/share/glib-2.0/schemas
}
